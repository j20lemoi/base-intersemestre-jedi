from enum import IntEnum, unique

@unique
class Direction(IntEnum):
    """
    Enum of the possible directions of the robot
    """
    LEFT = 1
    FRONT = 2
    RIGHT = 3
    BACK = 4
    