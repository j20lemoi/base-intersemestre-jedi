SENSOR_SAMPLING_RATE = 10 * 1e-3

LOW_HIGH_VALUES = {
    "low_front":   35.,   "low_left":  11.,  "low_right":  13.,
    "high_front": 75.,   "high_left": 50.,  "high_right": 50.
}

TIME_FOR_FULL_TURN = 8

COEFF_ADJUST_FULL_TURN = 0.15
TIME_FORWARD_AFTER_ADJUST = 0.1

PROPORTION_OF_TIME_BACKWARDS_FOR_TURN = 0.6

SEUL_AUTO_ADJUST_DIFFERENCE_SENSORS = 10