"""
File: controller.py
Description: Provides a PID controller for the robot that deals with motor speed
"""

from enum import IntEnum, unique
from math import inf

from threading import Thread
from .bricolage import COEFF_ADJUST_FULL_TURN, PROPORTION_OF_TIME_BACKWARDS_FOR_TURN, SEUL_AUTO_ADJUST_DIFFERENCE_SENSORS, TIME_FOR_FULL_TURN, TIME_FORWARD_AFTER_ADJUST

from .motor import Motor
from .sensors import HCSR04, HCSR04Event, ContinuousSensor
from .direction import Direction
from time import time, sleep
import random as rd

adjust_distance_memory = {"front_sensor": [], "left_sensor": [], "right_sensor": []}
raw_distance_memory = {"front_sensor": [], "left_sensor": [], "right_sensor": []}


@unique
class IntersectionType(IntEnum):
    """
    Possible intersection type encountered in the maze
    """
    PathFour = 0
    PathThreeLeftFront = 1
    PathThreeRightFront = 2
    PathThreeLeftRight = 3
    PathTwoLeft = 4
    PathTwoRight = 5
    PathTwoFront = 6
    PathOne = 7
    PathZero = 8


def danger(le: HCSR04Event, fe: HCSR04Event, re: HCSR04Event):
    """
    Returns True if one sensor among the three ones measures a distance
    below its lower threshold, False otherwise
    """
    return (le is HCSR04Event.DANGER or fe is HCSR04Event.DANGER
            or re is HCSR04Event.DANGER)


def is_exit(le: HCSR04Event, fe: HCSR04Event, re: HCSR04Event):
    """
    Returns True if the maze exit is reached, False otherwise
    """
    #TODO

    #Exit = NOTHING à gauche, devant et à droite
    return (le is HCSR04Event.NOTHING and fe is HCSR04Event.NOTHING and re is HCSR04Event.NOTHING)


def is_intersection(le: HCSR04Event, fe: HCSR04Event, re: HCSR04Event):
    """
    returns True if the droid is currently in an intersection, False otherwise
    """
    #TODO

    #S'il y a une ouverture à gauche ou à droite du robot, c'est une intersection
    return (le is HCSR04Event.NOTHING or re is HCSR04Event.NOTHING)


def is_dead_end(le: HCSR04Event, fe: HCSR04Event, re: HCSR04Event):
    """
    Returns True if the droid is in a dead end, False otherwise
    """
    #TODO

    # DANGER ou not NOTHING ? --> voir la sensibilité des capteurs
    return (le is not HCSR04Event.NOTHING and fe is HCSR04Event.DANGER and re is not HCSR04Event.NOTHING)


def is_corridor(le: HCSR04Event, fe: HCSR04Event, re: HCSR04Event):
    """
    Returns True if the Droid is in a corridor, False otherwise
    """
    #TODO

    # Murs à droite et à gauche mais pas devant
    # le et re : DANGER, WALL ou not NOTHING ? --> voir la sensibilité des capteurs
    # fe : NOTHING ou not DANGER ?
    return (le is not HCSR04Event.NOTHING and re is not HCSR04Event.NOTHING and fe is not HCSR04Event.DANGER)


def get_intersection_type(le, fe, re):
    """
    returns an IntersectionType according to the measured values on the
    three sensors
    """
    if is_exit(le, fe, re):
        return IntersectionType.PathFour
    if is_intersection(le, fe, re):
        if le is HCSR04Event.NOTHING and fe is HCSR04Event.NOTHING and re is not HCSR04Event.NOTHING:
            return IntersectionType.PathThreeLeftFront
        if (le is not HCSR04Event.NOTHING and fe is HCSR04Event.NOTHING
                and re is HCSR04Event.NOTHING):
            return IntersectionType.PathThreeRightFront
        if (le is HCSR04Event.NOTHING and fe is not HCSR04Event.NOTHING
                and re is HCSR04Event.NOTHING):
            return IntersectionType.PathThreeLeftRight
        if (le is HCSR04Event.NOTHING and fe is not HCSR04Event.NOTHING
                and re is not HCSR04Event.NOTHING):
            return IntersectionType.PathTwoLeft
        if (le is not HCSR04Event.NOTHING and fe is not HCSR04Event.NOTHING
                and re is HCSR04Event.NOTHING):
            return IntersectionType.PathTwoRight
        else:
            raise ValueError("Unknown situation")
    elif is_dead_end(le, fe, re):
        return IntersectionType.PathOne
    elif is_corridor(le, fe, re):
        return IntersectionType.PathTwoFront
    else:
        return IntersectionType.PathZero


class Controller:
    """
    Class to control motors according to sensed values.
    """

    def __init__(self,
                 motors: Motor,
                 lsensor: ContinuousSensor,
                 fsensor: ContinuousSensor,
                 rsensor: ContinuousSensor,
                 speed=0.5):
        self.motors = motors
        self.speed = speed
        self.left_sensor = lsensor
        self.front_sensor = fsensor
        self.right_sensor = rsensor

    def start(self):
        """
        Starts the threads for computing the distance of the sensor
        """
        for sensor in [self.left_sensor, self.front_sensor, self.right_sensor]:
            sensor.start()

    def stop(self):
        """
        Starts the threads for computing the distance of the sensor
        """
        for sensor in [self.left_sensor, self.front_sensor, self.right_sensor]:
            sensor.stop()

    def _get_event(self, sensor: ContinuousSensor, moving=True):
        """
        Returns a HCSR04Event according to the distance measured by the sensor
        :param bool moving: Indicates if the robot is moving
        """
        which_sensor = "left_sensor" if sensor == self.left_sensor else ("right_sensor" if sensor == self.right_sensor else "front_sensor")
        distance = self._get_robust_distance(sensor, which_sensor, moving)
        if distance < sensor.sensor.low:
            return (HCSR04Event.DANGER, distance)
        if distance < sensor.sensor.high:
            return (HCSR04Event.WALL, distance)
        return (HCSR04Event.NOTHING, distance)

    def _get_robust_distance(self, sensor: HCSR04, which_sensor, moving=True):
        """
        Robust measurement on sensor, stops motors if necessary and restart
        them according to an acceptable measurement.
        """
        #TODO Implement a robust way to read the distance from the sensor
        global raw_distance_memory

        cache = raw_distance_memory[which_sensor][-5::]
        print("robust", which_sensor, cache)
        cache = [d for d in cache if d > 1]

        if len(cache) < 3:
            return cache[-1]
        return min(cache)

    def startThreadReadingSensors(self):
        def aux():
            while True:
                raw_distance_memory["left_sensor"].append(self.left_sensor.sensor.get_distance())
                raw_distance_memory["right_sensor"].append(self.right_sensor.sensor.get_distance())
                raw_distance_memory["front_sensor"].append(self.front_sensor.sensor.get_distance())
                sleep(self.front_sensor.sampling_rate)
        thread = Thread(target=aux)
        thread.start()
    def _get_intersection_type(self):
        """
        Return intersection type using self sensors and controller.py get_intersection_type
        """
        le = self._get_event(self.left_sensor)[0]
        fe = self._get_event(self.front_sensor)[0]
        re = self._get_event(self.right_sensor)[0]

        return get_intersection_type(le, fe, re)


class PIDController(Controller):
    """
    A PIDController manages the speed of the motors according to the PID paradigm.
    Read https://en.wikipedia.org/wiki/PID_controller and
    https://projects.raspberrypi.org/en/projects/robotPID/5
    """

    def __init__(self,
                 motors: Motor,
                 lsensor: ContinuousSensor,
                 fsensor: ContinuousSensor,
                 rsensor: ContinuousSensor,
                 speed=0.7,
                 target=20.,
                 KP=0.05,
                 KD=0.005,
                 KI=0.,
                 pid_sample_time=0.01):
        Controller.__init__(self,
                            motors,
                            lsensor,
                            fsensor,
                            rsensor,
                            speed=speed)
        self.m1_speed = self.speed
        self.m2_speed = self.speed
        self.target = target
        # https://en.wikipedia.org/wiki/PID_controller
        self.pid_sample_time = pid_sample_time
        self.last_sample = 0.0
        self.KP = KP
        self.KD = KD
        self.KI = KI
        self.prev_error = 0.0
        self.sum_error = 0.0

    def adjust(self):
        """
        Adjusts motors speeds to compensate error on objective function
        (namely a distance on left or right wall)
        """
        global adjust_distance_memory
        #TODO Implement

        events = {
            "left_sensor": self._get_event(self.left_sensor),
            "right_sensor": self._get_event(self.right_sensor)
        }
        distances = {
            "left_sensor": events["left_sensor"][1],
            "right_sensor": events["right_sensor"][1]
        }

        closest_sensor = "left_sensor"
        adjust_direction = Direction.RIGHT
        if distances["left_sensor"] > distances["right_sensor"]:
            closest_sensor = "right_sensor"
            adjust_direction = Direction.LEFT

        if events[closest_sensor] is HCSR04Event.DANGER:
            if distances[closest_sensor] < 2:
                self.motors.stop()
                while self._get_event(self[closest_sensor])[1] < 2:
                    pass
                self.motors.forward()

            self.turn(adjust_direction, coeff=COEFF_ADJUST_FULL_TURN)
            self.motors.forward()
            sleep(TIME_FORWARD_AFTER_ADJUST)

        elif distances["left_sensor"] != inf and distances["right_sensor"] != inf:
            distanceDiff = distances["left_sensor"] - distances["right_sensor"]
            if abs(distanceDiff) > SEUL_AUTO_ADJUST_DIFFERENCE_SENSORS:
                if len(adjust_distance_memory[closest_sensor]) > 0 \
                        and distances[closest_sensor] < adjust_distance_memory[closest_sensor][-1]:
                    self.turn(adjust_direction, coeff=COEFF_ADJUST_FULL_TURN)
                    self.motors.forward()
                    sleep(TIME_FORWARD_AFTER_ADJUST)

            adjust_distance_memory["left_sensor"].append(distances["left_sensor"])
            adjust_distance_memory["right_sensor"].append(distances["right_sensor"])


    def turn(self, turnDirection, coeff=1):
        #print("turn", turnDirection, coeff)
        turn360 = TIME_FOR_FULL_TURN
        if turnDirection is Direction.LEFT:
            self.motors.left()
        elif turnDirection  is Direction.RIGHT:
            self.motors.right()
        else:
            if rd.random() > 0.5:
                self.motors.left()
            else:
                self.motors.right()
            sleep(coeff * turn360/4)
        sleep(coeff * turn360/4)

    def correctRotation(self, turnDirection):
        #print("correctRotation", turnDirection)
        other_sensor = self.left_sensor if turnDirection is Direction.RIGHT else self.right_sensor

        if self._get_event(self.front_sensor)[0] is HCSR04Event.DANGER:
            old_other_sensor_dist = self._get_event(other_sensor)[1]
            new_other_sensor_dist = old_other_sensor_dist
            self.motors.left() if turnDirection is Direction.LEFT else self.motors.right()
            while new_other_sensor_dist <= old_other_sensor_dist:
                #print("On est bloqué")
                old_other_sensor_dist = new_other_sensor_dist
                new_other_sensor_dist = self._get_event(other_sensor)[1]
        self.motors.forward()
        #print("ON SORT DE LA")



    def turnOperation(self, turnDirection):
        #print("turnOperation", turnDirection)
        sideSensor = self.left_sensor if turnDirection is Direction.LEFT else self.right_sensor

        self.motors.forward()  # potentiellement inutile, car on va déjà devant
        tStart = time()
        while not (self._get_event(self.front_sensor)[0] is HCSR04Event.DANGER or self._get_event(sideSensor)[
            0] is not HCSR04Event.NOTHING):
            pass
        tStop = time()
        self.motors.stop()

        if self._get_event(sideSensor)[0] is not HCSR04Event.NOTHING:
            tForward = tStop - tStart
            tBackward = tForward * PROPORTION_OF_TIME_BACKWARDS_FOR_TURN

            self.motors.backward()
            sleep(tBackward)

        self.turn(turnDirection)

        #print("coucou")
        self.motors.forward()
        type_intersection = self._get_intersection_type()
        while (type_intersection is not IntersectionType.PathTwoFront) and (type_intersection is not IntersectionType.PathOne):
            self.correctRotation(turnDirection)
            type_intersection = self._get_intersection_type()
            #print("blocked type", type_intersection)
        #print("bye coucou")


    def go_to_the_next_intersection(self, nextDirection):
        """
        Drives the droid 'safely' to the next intersection.
        Safely meaning that the droids to not meet a wall and stops when it detects
        an IntersectionType that is not PathTwoFront, i.e., the droid is not in 
        a corridor anymore
        """
        #print("nextIntersection", nextDirection)
        if nextDirection is Direction.FRONT:
            pass
        elif nextDirection is Direction.BACK:
            self.turn(nextDirection)
        else:
            self.turnOperation(nextDirection)

        self.motors.forward() # Une fois qu'on a fini la manœuvre, on va tout droit et on sort de la fonction